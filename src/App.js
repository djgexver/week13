import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
        Hello! My_name is Vladislavs Kergets. Student_Code: 161REB153.
        </a>
        <img src="https://sun9-62.userapi.com/c855224/v855224863/235014/r-eKZaQxN3E.jpg" width="50%" height="50%" alt="MyPhoto"/>
      </header>
    </div>
  );
}

export default App;
